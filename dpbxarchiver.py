#! /usr/bin/env python3
# encoding: utf-8

'''
dpbxarchiver.py

Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2018 Jason Noennig

This file is part of dpbxarchiver.

    dpbxarchiver is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    dpbxarchiver is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with dpbxarchiver.  If not, see <http://www.gnu.org/licenses/>.

'''


import argparse
import datetime
import json
import os
from pathlib import Path
from pathlib import PurePath
import time
import sys
import logging
import gzip
from timeit import default_timer as timer
import multiprocessing as mp

import dropbox

__version__ = '0.1a'


def get_args():
    '''
    Command line arguments.

    Returns:
        args
    '''
    parser = argparse.ArgumentParser(description='Archives remote photos on Dropbox to a local drive')
    parser.add_argument('-lp', '--localpath', required=True, action='store',
                        help='The path of the local directory')
    parser.add_argument('-kf', '--keyfile', required=True, action='store',
                        help='The path to the Dropbox API JSON key file')
    parser.add_argument('-rp', '--remotepath', required=False, default='/Camera Uploads', action='store',
                        help='Dropbox remote path')
    parser.add_argument('-ll', '--logginglevel', required=False, default='INFO', action='store',
                        help='Specifies log level (DEBUG, INFO, WARNING and ERROR)')
    parser.add_argument('-V', '--version', action='version', version='%(prog)s ' + __version__,
                        help='Shows version number of script')
    args = parser.parse_args()
    return args


def get_token(keyfile):
    '''get_token gets the Dropbox api key from a JSON file

    Parameters:
        keyfile = The json file the key is stored
            Example keyfile:
                {
                    "token" : "my Dropbox api key"
                }

    Returns:
        token (str)
    '''
    logging.debug('Starting get_token func')
    try:
        with open(keyfile) as json_data:
            token = json.load(json_data)
        token = token['token']
        return token
    except Exception as myerror:
        logging.error(myerror)


class Archiver(object):
    '''
    Archiver class gets and compares local and Dropbox files
    '''
    
    def __init__(self, local_path, remote_path):
        '''
        '''
        self.local_path = local_path
        self.remote_path = remote_path
    
    class ArchiveError(Exception):
        '''
        Passes errors
        '''
        pass

    def get_local_files(self):
        '''Gets the local files to compare against Dropbox files

        Parameters:
            local_path: Local directory used to store Dropbox archive

        Returns:
            local_files_dict dictionary returns files and their associated
            modified time and size
            format: {'file name': {'mtime': datetime_object,
                     'size': integer size in bytes}}
        '''
        logging.debug('Starting get_local_files func')
        try:
            local_files_list = [f for f in self.local_path.iterdir() if Path(PurePath(self.local_path, f)).is_file]
            local_files_dict = {}
            logging.debug('Staring loop of local files in get_local_files func')
            for myfile in local_files_list:
                filename = str(myfile).split('/')[-1]
                mtime = myfile.stat().st_mtime
                mtime_dt = datetime.datetime(*time.gmtime(mtime)[:6])
                size = int(myfile.stat().st_size)
                local_files_dict[filename] = {'mtime': mtime_dt, 'size': size}
            return local_files_dict
        except self.ArchiveError as e:
            print(e.args)
            logging.error(e)


    def get_remote_files(self):
        '''Gets all the remote files in the remote_path variable from Dropbox

        Parameters:
            remote_path: Dropbox folder you want to archive (default is "/Camera Uploads")

        Returns:
            remote_files_dict dictionany returns files and their associated metadata
        '''
        logging.debug('Starting get_remote_files func')
        try:
            remote_files = DBX.files_list_folder(str(self.remote_path))
            remote_files_dict = {}
            logging.debug('Staring loop of remote files in get_remote_files func')
            for entry in remote_files.entries:
                remote_files_dict[entry.name] = entry
            while remote_files.has_more:
                remote_files = DBX.files_list_folder_continue(remote_files.cursor)
                for entry in remote_files.entries:
                    remote_files_dict[entry.name] = entry
            return remote_files_dict
        except self.ArchiveError as e:
            print(e.args)
            logging.error(e)

    def create_paths(self, remote_file):
        '''
        Creates a tuple of the local_fullpath and remote_fullpath
        It is used by the download function
        '''
        local_fullpath = PurePath(self.local_path, remote_file)
        remote_fullpath = PurePath(self.remote_path, remote_file)
        return (local_fullpath, remote_fullpath)
    
    def dwnld_files(self, local_files_dict, remote_files_dict):
        '''dwnld_files compares the local_files_dict and remote_files_dict
        and creates a list of files to be downloaded

        Parameters:
            local_files_dict: Is the dictonary returned from get_local_files func
            remote_files_dict: Is the dictonary returned from get_remote_files func

        Returns:
            dwnld_files_list which contains the tuples (local_path, remote_path)
        '''
        logging.debug('Starting dwnld_files func')
        try:
            dwnld_files_list = []
            logging.debug('Staring loop of file comparison in dwnld_files func')
            for remotefile, remotevalues in remote_files_dict.items():
                if remotefile not in local_files_dict:
                    dwnld_files_list.append(self.create_paths(remotefile))
                else:
                    if remotevalues.size != local_files_dict[remotefile]['size']:
                        dwnld_files_list.append(self.create_paths(remotefile))
                        if remotevalues.client_modified != local_files_dict[remotefile]['mtime']:
                            dwnld_files_list.append(self.create_paths(remotefile))
            if dwnld_files_list:
                logging.info('Files to be downloaded:')
                logging.info(', '.join([str(x) for x in dwnld_files_list]))
            logging.info('Number of files to be downloaded: {}'.format(len(dwnld_files_list)))
            return dwnld_files_list
        except self.ArchiveError as e:
            print(e.args)
            logging.error(e)


def download(remote_file):
    '''downloads the files from Dropbox

    Parameters:
        remote_file: The Dropbox file to be downloaded

    Returns:
        Nothing
    '''
    logging.debug('Starting download func')
    try:
        local_fullpath = str(remote_file[0])
        remote_fullpath = str(remote_file[1])
        result = DBX.files_download_to_file(local_fullpath, remote_fullpath)
    except dropbox.exceptions.HttpError as err:
        logging.error('*** HTTP error {}'.format(err))
        print(err)
        return None


def log_rotate(log_path, log_file):
    '''log_rotate checks the size of the log file and will
    compress the existing log file if it is over 2 MB. After
    it gzips the original log file it will delete the original
    '''
    logging.debug('Starting log_rotate function')
    try:
        log_pf = PurePath(log_path, log_file)
        log_pf = Path(log_pf)
        if log_pf.exists():
            current_time = datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%dT%H%M%S')
            gzip_file = PurePath(log_path, current_time, '-', log_file, '.gz')
            byte_size = log_pf.stat().st_size
            if byte_size > 2097152:
                f_in = open(log_pf, 'rb')
                f_out = gzip.open(gzip_file, 'wb')
                f_out.writelines(f_in)
                f_out.close()
                f_in.close()
                os.remove(log_pf)
        else:
            logging.error('{} does not exist'.format(log_pf))
    except Exception as e:
        logging.error(e)


def main():
    '''
    Main Program
    '''
    args = get_args()
    start_timer = timer()
    home_dir = os.path.expanduser('~')
    log_path = PurePath(home_dir, 'logs/dpbxarchiver')
    log_path = Path(log_path)
    if not log_path.is_dir():
        log_path.mkdir()
    log_file = 'dpbxarchiver.log'
    log_pf = PurePath(log_path, log_file)
    log_pf = str(log_pf)
    logging.basicConfig(filename=log_pf,
                        format='%(asctime)s %(levelname)s: %(message)s',
                        level=args.logginglevel)
    logging.info('Starting dpbxarchiver.py')
    key_file = Path(args.keyfile)
    if key_file.exists():
        mytoken = get_token(args.keyfile)
        # Declared globally so it can be used by the download func
        global DBX
        DBX = dropbox.Dropbox(mytoken)
        remote_path = Path(args.remotepath)
        local_path = Path(args.localpath)
        if local_path.exists():
            archiver = Archiver(local_path, remote_path)
            local_files_dict = archiver.get_local_files()
            remote_files_dict = archiver.get_remote_files()
            dwnld_files_list = archiver.dwnld_files(local_files_dict, remote_files_dict)
            pool = mp.Pool(5)
            pool.map(download, dwnld_files_list)
            log_rotate(log_path, log_file)
            end_timer = timer()
            logging.info('dpbxarchiver.py took ' + str(end_timer - start_timer) + ' seconds to run.')
        else:
            logging.critical(str(local_path) + ' does NOT exist. dpbxarchiver.py is exiting')
    else:
        logging.critical(str(key_file) + ' does NOT exist. dpbxarchiver.py is exiting')
        sys.exit()


if __name__ == '__main__':
    main()
