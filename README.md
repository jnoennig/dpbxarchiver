dpbxarchiver
============
The Dropbox desktop app currently does not support running on ARM processors. I created dpbxarchiver to archive photos from a Dropbox account to a local hard drive. I used a Raspberry Pi with an external USB drive.

I use a cronjob to run dpbxarchiver.py, it then collects the local files in your selected repository and compares them to the files in your Dropbox selected folder (Default: "Camera Uploads"). If a new file has been added or a file in Dropbox has been modified it will download the file to your local repository. 

## Prerequisites
* Python 3.4+
* dpbxarchiver.py has only been tested on Linux
* Local hard drive with enough capacity to store your data.
* Dropbox API key

## My Workflow
1. Take photos on my phone.
2. Dropbox phone app syncs photos to Dropbox.
3. Cronjob running dpbxarchiver.py every 3 hours checks for changes and downloads any new or updated photos.

## Flags
### Required
* -lp or --localpath
    * local path where the script will download the files
* -kf or --keyfile
    * Dropbox API keyfile

### Optional
* -rp or --remotepath
    * Default: "/Camera Uploads"
    * Dropbox folder to download from
* -ll or --logginglevel
    * Default: INFO
    * You can set the logging level of the script (DEBUG, INFO, WARNING and ERROR)


## Directories and Logs
dpbxarchiver.py will create a logs directory and log files. It handles the log rotation of the log files.
```
/home/alice/logs/dpbxarchiver/dpbxarchiver.log
```

## Example of Cronjob
```
0   0,3,6,9,12,15,18,21 * * *  /usr/bin/python3 /home/alice/scripts/dpbxarchiver/dpbxarchiver.py -kf /home/alice/.aks/dpbx.json -lp /mnt/myextHD/dropboxArchive/
```

## Example of Dropbox JSON Keyfile
```
{
    "token": "<YOUR DROPBOX API KEY>"
}
```

All product and company names are trademarks™ or registered® trademarks of 
their respective holders. Use of them does not imply any affiliation with 
or endorsement by them.